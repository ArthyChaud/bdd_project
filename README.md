# Question 1 :

Utiliser les test dans `src/test/all/` ou utiliser le fichier `src/test/RunAllJdbcTests.java`

# Question 2 à 11 :

Utiliser les tests dans `src/test/queries/`

Les question 6 et 10 on été faites sans SQL.

# Autres info

Pour construire la même base de donnée que la notre il y a des script SQL dans le dossier `sql/`