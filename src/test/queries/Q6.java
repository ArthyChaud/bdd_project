package test.queries;

import dao.exception.DaoException;
import dao.jdbc.MarqueDaoImpl;
import model.Queries;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q6 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      new MarqueDaoImpl(connection).howManyVehiculesByBrand().forEach((k, v) -> System.out.println(k + " : " + v));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    queries.howManyVehiculesByBrand().forEach((k, v) -> System.out.println(k + " : " + v));

     */
  }
}
