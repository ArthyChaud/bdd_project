package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import dao.jdbc.ClientDaoImpl;
import dao.jdbc.ContratDaoImpl;
import dao.jdbc.VehiculeDaoImpl;
import model.Queries;
import model.entity.*;
import sql.PostgresConnection;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

public class Q3 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      ContratDaoImpl contratDao = new ContratDaoImpl(connection);
      List<Entity> contrats = (List<Entity>) new ContratDaoImpl(connection).findAll();
      contratDao.returnVehicule(
          (Contrat) contratDao.findById(((Contrat) contrats.get(contrats.size() - 1)).getIdContrat())
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    AgenceDaoImpl agenceDaoImpl = new AgenceDaoImpl(connection);
    Contrat contrat = new Contrat();
    try {
      contrat = queries.rent(
          (Client) new ClientDaoImpl(connection).findById(1),
          new Date(Calendar.getInstance().getTime().getTime()),
          new Date(0, 2, 12),
          (Agence) agenceDaoImpl.findById(2),
          (Agence) agenceDaoImpl.findById(1)
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    Vehicule vehicule = contrat.getVehicule();
    vehicule.setNbKilometres(vehicule.getNbKilometres() + 150);
    queries.returnVehicule(contrat);
    System.out.println(contrat);

     */
  }
}
