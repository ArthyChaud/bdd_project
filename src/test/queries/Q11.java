package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import model.Queries;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q11 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      new AgenceDaoImpl(connection).turnoverByAgence().forEach((k, v) -> System.out.println(k + " : " + v));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    queries.turnoverByAgence().forEach((k, v) -> System.out.println(k + " : " + v));

     */
  }
}
