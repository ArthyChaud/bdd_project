package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import dao.jdbc.ClientDaoImpl;
import model.Queries;
import model.entity.*;
import sql.PostgresConnection;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;

public class QueriesTests {
  public static void main(String[] args) {
    System.out.println("*** 2 ***");
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    AgenceDaoImpl agenceDaoImpl = new AgenceDaoImpl(connection);
    Contrat contrat = new Contrat();
    try {
      contrat = queries.rent(
          (Client) new ClientDaoImpl(connection).findById(1),
          new Date(Calendar.getInstance().getTime().getTime()),
          new Date(0, 2, 12),
          (Agence) agenceDaoImpl.findById(2),
          (Agence) agenceDaoImpl.findById(1)
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    System.out.println(contrat);
    System.out.println("*** 3 ***");
    Vehicule vehicule = contrat.getVehicule();
    vehicule.setNbKilometres(vehicule.getNbKilometres() + 150);
    queries.returnVehicule(contrat);
    System.out.println(contrat);
    System.out.println("*** 4 ***");
    System.out.println(queries.newFacture(contrat));
    System.out.println("*** 5 ***");
    try {
      System.out.println(queries.getTurnover((Agence) agenceDaoImpl.findById(1), Calendar.FEBRUARY));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    System.out.println("*** 6 ***");
    queries.howManyVehiculesByBrand().forEach((k, v) -> System.out.println(k + " : " + v));
    System.out.println("*** 7 ***");
    try {
      System.out.println(queries.theOneWhoHasMadeTheMostRents((Agence) agenceDaoImpl.findById(2), 2020));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    System.out.println("*** 8 ***");
    queries.turnoverByCategories().forEach((k, v) -> System.out.println(k + " : " + v));
    System.out.println("*** 9 ***");
    queries.turnoverByTypes().forEach((k, v) -> System.out.println(k + " : " + v));
    System.out.println("*** 10 ***");
    queries.nbOldVehiculeByAgence().forEach((k, v) -> System.out.println(k + " : " + v));
    System.out.println("*** 11 ***");
    queries.turnoverByAgence().forEach((k, v) -> System.out.println(k + " : " + v));
  }
}
