package test.queries;

import dao.exception.DaoException;
import dao.jdbc.CategorieDaoImpl;
import model.Queries;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q8 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      new CategorieDaoImpl(connection).turnoverByCategories().forEach((k, v) -> System.out.println(k + " : " + v));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    queries.turnoverByCategories().forEach((k, v) -> System.out.println(k + " : " + v));
     */
  }
}
