package test.queries;

import model.Queries;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q10 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    queries.nbOldVehiculeByAgence().forEach((k, v) -> System.out.println(k + " : " + v));
  }
}
