package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import dao.jdbc.ClientDaoImpl;
import dao.jdbc.ContratDaoImpl;
import dao.jdbc.VehiculeDaoImpl;
import model.Queries;
import model.entity.Agence;
import model.entity.Client;
import model.entity.Contrat;
import model.entity.Vehicule;
import sql.PostgresConnection;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;

public class Q2 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      new ContratDaoImpl(connection).rent(
          (Client) new ClientDaoImpl(connection).findById(1),
          new Date(2020, 1, 1),
          new Date(2020, 1, 1),
          (Vehicule) new VehiculeDaoImpl(connection).findById("AK001AA"),
          (Agence) new AgenceDaoImpl(connection).findById(1),
          (Agence) new AgenceDaoImpl(connection).findById(1)
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    AgenceDaoImpl agenceDaoImpl = new AgenceDaoImpl(connection);
    Contrat contrat = new Contrat();
    try {
      contrat = queries.rent(
          (Client) new ClientDaoImpl(connection).findById(1),
          new Date(Calendar.getInstance().getTime().getTime()),
          new Date(0, 2, 12),
          (Agence) agenceDaoImpl.findById(2),
          (Agence) agenceDaoImpl.findById(1)
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    System.out.println(contrat);

     */
  }
}
