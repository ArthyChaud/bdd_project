package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import model.Queries;
import model.entity.Agence;
import sql.PostgresConnection;

import java.sql.Connection;
import java.util.Calendar;

public class Q5 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    AgenceDaoImpl agenceDaoImpl = new AgenceDaoImpl(connection);
    try {
      System.out.println(queries.getTurnover((Agence) agenceDaoImpl.findById(1), Calendar.FEBRUARY));
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
