package test.queries;

import dao.exception.DaoException;
import dao.jdbc.TypeDaoImpl;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q9 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      new TypeDaoImpl(connection).turnoverByTypes().forEach((k, v) -> System.out.println(k + " : " + v));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    queries.turnoverByTypes().forEach((k, v) -> System.out.println(k + " : " + v));

     */
  }
}
