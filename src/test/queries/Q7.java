package test.queries;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import dao.jdbc.ClientDaoImpl;
import model.Queries;
import model.entity.Agence;
import sql.PostgresConnection;

import java.sql.Connection;

public class Q7 {
  public static void main(String[] args) {
    Connection connection = PostgresConnection.getInstance();
    try {
      System.out.println(new ClientDaoImpl(connection).theOneWhoHasMadeTheMostRents(
          (Agence) new AgenceDaoImpl(connection).findById(2), 2020)
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
    /*
    Connection connection = PostgresConnection.getInstance();
    Queries queries = new Queries(connection);
    AgenceDaoImpl agenceDaoImpl = new AgenceDaoImpl(connection);
    try {
      System.out.println(queries.theOneWhoHasMadeTheMostRents((Agence) agenceDaoImpl.findById(2), 2020));
    } catch (DaoException e) {
      e.printStackTrace();
    }
     */
  }
}
