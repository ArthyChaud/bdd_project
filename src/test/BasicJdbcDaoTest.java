package test;

import dao.Dao;
import dao.exception.DaoException;
import model.entity.Entity;
import sql.PostgresConnection;

import java.sql.Connection;

public abstract class BasicJdbcDaoTest {
  protected Connection connection;
  protected Dao dao;

  protected BasicJdbcDaoTest() {
    connection = PostgresConnection.getInstance();
  }

  protected abstract void testFindAll();
  protected abstract void testFindById(int id);
  protected abstract void testCreate(Entity entity);
  protected abstract void testUpdate(Entity entity);
  protected abstract void testDelete(Entity entity);

  public abstract void test() throws DaoException;
}
