package test;

import test.all.*;

import java.util.Arrays;

public class RunAllJdbcTests {

  public static void main(String[] args) {
    Arrays.asList(
        new VilleJdbcDaoTest(),
        new ClientJdbcDaoTest(),
        new AgenceJdbcDaoTest(),
        new CategorieJdncDaoTest(),
        new TypeJdbcDaoTest(),
        new ModeleJdbcDaoTest(),
        new MarqueJdbcDaoTest(),
        new VehiculeJdbcDaoTest(),
        new ContratJdbcDaoTest(),
        new FactureJdbcDaoTest()
    ).forEach(basicJdbcDaoTest -> {
      try {
        basicJdbcDaoTest.test();
      } catch (dao.exception.DaoException e) {
        e.printStackTrace();
      }
    });
  }
}
