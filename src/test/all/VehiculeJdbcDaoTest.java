package test.all;

import dao.exception.DaoException;
import dao.jdbc.VehiculeDaoImpl;
import model.entity.Entity;
import model.entity.Vehicule;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class VehiculeJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Vehicule v = (Vehicule) e;
        System.out.println(
            v.getImmatriculation() + " | " +
            v.getDateMiseEnCirculation() + " | " +
            v.getEtat() + " | " +
            v.getNbKilometres() + " | " +
            v.getPrixParJourDeLocation() + " | " +
            v.getMarque().getIdMarque() + " | " +
            v.getModele().getIdModele() + " | " +
            v.getCategorie().getIdCategorie() + " | " +
            v.getType().getIdType() + " | " +
            v.getAgence().getIdAgence()
        );
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    testFindById(Integer.toString(id));
  }

  protected void testFindById(String id) {
    try {
      Vehicule vehicule = (Vehicule) ((VehiculeDaoImpl) dao).findById(id);
      System.out.println(
          vehicule.getImmatriculation() + " | " +
          vehicule.getDateMiseEnCirculation() + " | " +
          vehicule.getEtat() + " | " +
          vehicule.getNbKilometres() + " | " +
          vehicule.getPrixParJourDeLocation() + " | " +
          vehicule.getMarque().getIdMarque() + " | " +
          vehicule.getModele().getIdModele() + " | " +
          vehicule.getCategorie().getIdCategorie() + " | " +
          vehicule.getType().getIdType() + " | " +
          vehicule.getAgence().getIdAgence()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Vehicule *****");
    dao = new VehiculeDaoImpl(connection);
    Vehicule vehicule;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById("AK001AA");
    System.out.println("**** create ****");
    vehicule = (Vehicule) ((VehiculeDaoImpl) dao).findById("AK001AA");
    vehicule.setImmatriculation(vehicule.getImmatriculation() + '1');
    testCreate(vehicule);
    System.out.println("**** update ****");
    List<Entity> vehicules = (ArrayList<Entity>) dao.findAll();
    vehicule = (Vehicule) ((VehiculeDaoImpl) dao).findById(
        ((Vehicule) vehicules.get(vehicules.size() - 1)).getImmatriculation()
    );
    vehicule.setNbKilometres(0);
    testUpdate(vehicule);
    System.out.println("**** delete ****");
    testDelete(vehicule);
  }

  public static void main(String[] args) {
    try {
      new VehiculeJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
