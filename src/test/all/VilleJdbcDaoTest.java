package test.all;

import dao.exception.DaoException;
import dao.jdbc.VilleDaoImpl;
import model.entity.Entity;
import model.entity.Ville;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class VilleJdbcDaoTest extends BasicJdbcDaoTest {

  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Ville v = (Ville) e;
        System.out.println(v.getIdVille() + " | " + v.getNomVille() + " | " + v.getNombreHabitants());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Ville ville = (Ville) dao.findById(id);
      System.out.println(ville.getIdVille() + " | " + ville.getNomVille() + " | " + ville.getNombreHabitants());
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Ville *****");
    dao = new VilleDaoImpl(connection);
    Ville ville;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    ville = (Ville) dao.findById(1);
    ville.setNomVille(ville.getNomVille() + '2');
    testCreate(ville);
    System.out.println("**** update ****");
    List<Entity> villes = (ArrayList<Entity>) dao.findAll();
    ville = (Ville) dao.findById(((Ville) villes.get(villes.size() - 1)).getIdVille());
    ville.setNomVille(ville.getNomVille() + '\b');
    testUpdate(ville);
    System.out.println("**** delete ****");
    testDelete(ville);
  }

  public static void main(String[] args) {
    try {
      new VilleJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
