package test.all;

import dao.exception.DaoException;
import dao.jdbc.ClientDaoImpl;
import model.entity.Client;
import model.entity.Entity;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class ClientJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Client c = (Client) e;
        System.out.println(
            c.getIdClient() + " | " +
            c.getNomClient() + " | " +
            c.getAdresseClient() + " | " +
            c.getCodePostalClient() + " | " +
            c.getVille().getIdVille()
        );
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Client client = (Client) dao.findById(id);
      System.out.println("" +
          client.getIdClient() + " | " +
          client.getNomClient() + " | " +
          client.getAdresseClient() + " | " +
          client.getCodePostalClient() + " | " +
          client.getVille().getIdVille()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Client *****");
    dao = new ClientDaoImpl(connection);
    Client client;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    client = (Client) dao.findById(1);
    client.setNomClient(client.getNomClient() + '2');
    testCreate(client);
    System.out.println("**** update ****");
    List<Entity> clients = (ArrayList<Entity>) dao.findAll();
    client = (Client) dao.findById(((Client) clients.get(clients.size() - 1)).getIdClient());
    client.setNomClient(client.getNomClient() + '\b');
    testUpdate(client);
    System.out.println("**** delete ****");
    testDelete(client);
  }

  public static void main(String[] args) {
    try {
      new ClientJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
