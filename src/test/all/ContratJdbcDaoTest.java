package test.all;

import dao.exception.DaoException;
import dao.jdbc.ContratDaoImpl;
import model.entity.Contrat;
import model.entity.Entity;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class ContratJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Contrat c = (Contrat) e;
        System.out.println(
            c.getIdContrat() + " | " +
            c.getDateDeRetrait() + " | " +
            c.getDateDeRetour() + " | " +
            c.getKmRetrait() + " | " +
            c.getKmRetour() + " | " +
            c.getClient().getIdClient() + " | " +
            c.getVehicule().getImmatriculation() + " | " +
            c.getAgenceDeRetour().getIdAgence()
        );
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Contrat contrat = (Contrat) dao.findById(id);
      System.out.println("" +
          contrat.getIdContrat() + " | " +
          contrat.getDateDeRetrait() + " | " +
          contrat.getDateDeRetour() + " | " +
          contrat.getKmRetrait() + " | " +
          contrat.getKmRetour() + " | " +
          contrat.getClient().getIdClient() + " | " +
          contrat.getVehicule().getImmatriculation() + " | " +
          contrat.getAgenceDeRetour().getIdAgence()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Contrat *****");
    dao = new ContratDaoImpl(connection);
    Contrat contrat;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    contrat = (Contrat) dao.findById(1);
    contrat.setKmRetour(contrat.getKmRetour() + 1);
    testCreate(contrat);
    System.out.println("**** update ****");
    List<Entity> contrats = (ArrayList<Entity>) dao.findAll();
    contrat = (Contrat) dao.findById(((Contrat) contrats.get(contrats.size() - 1)).getIdContrat());
    contrat.setKmRetour(contrat.getKmRetour() - 1);
    testUpdate(contrat);
    System.out.println("**** delete ****");
    testDelete(contrat);
  }

  public static void main(String[] args) {
    try {
      new ContratJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
