package test.all;

import dao.exception.DaoException;
import dao.jdbc.ModeleDaoImpl;
import model.entity.Entity;
import model.entity.Modele;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class ModeleJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Modele m = (Modele) e;
        System.out.println(m.getIdModele() + " | " + m.getDenomination() + " | " + m.getPuissanceFiscale());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Modele modele = (Modele) dao.findById(id);
      System.out.println(
          modele.getIdModele() + " | " + modele.getDenomination() + " | " + modele.getPuissanceFiscale()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Modele *****");
    dao = new ModeleDaoImpl(connection);
    Modele modele;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    modele = (Modele) dao.findById(1);
    modele.setDenomination(modele.getDenomination() + '1');
    testCreate(modele);
    System.out.println("**** update ****");
    List<Entity> modeles = (ArrayList<Entity>) dao.findAll();
    modele = (Modele) dao.findById(((Modele) modeles.get(modeles.size() - 1)).getIdModele());
    modele.setDenomination(modele.getDenomination() + '\b');
    testUpdate(modele);
    System.out.println("**** delete ****");
    testDelete(modele);
  }

  public static void main(String[] args) {
    try {
      new ModeleJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
