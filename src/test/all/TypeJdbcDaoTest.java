package test.all;

import dao.exception.DaoException;
import dao.jdbc.TypeDaoImpl;
import model.entity.Entity;
import model.entity.Type;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class TypeJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
        try {
      dao.findAll().forEach(e -> {
        Type t = (Type) e;
        System.out.println(t.getIdType() + " | " + t.getLibelleType());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }

  }

  @Override
  protected void testFindById(int id) {
    try {
      Type type = (Type) dao.findById(id);
      System.out.println(type.getIdType() + " | " + type.getLibelleType());
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
   try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }

  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Type *****");
    dao = new TypeDaoImpl(connection);
    Type type;
    System.out.println("***** findAll *****");
    testFindAll();
    System.out.print("***** findById *****");
    testFindById(1);
    System.out.println("***** create *****");
    type = (Type) dao.findById(1);
    type.setLibelleType(type.getLibelleType() + '2');
    testCreate(type);
    System.out.println("**** update ****");
    List<Entity> types = (ArrayList<Entity>) dao.findAll();
    type = (Type) dao.findById(((Type) types.get(types.size() - 1)).getIdType());
    type.setLibelleType(type.getLibelleType() + '\b');
    testUpdate(type);
    System.out.println("***** delete *****");
    testDelete(type);
  }

  public static void main(String[] args) {
    try {
      new TypeJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
