package test.all;

import dao.exception.DaoException;
import dao.jdbc.AgenceDaoImpl;
import model.entity.Agence;
import model.entity.Entity;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class AgenceJdbcDaoTest extends BasicJdbcDaoTest {

  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Agence a = (Agence) e;
        System.out.println(a.getIdAgence() + " | " + a.getNbEmployes() + " | " + a.getVille().getIdVille());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Agence agence = (Agence) dao.findById(id);
      System.out.println(
          agence.getIdAgence() + " | " + agence.getNbEmployes() + " | " + agence.getVille().getIdVille()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Agence *****");
    dao = new AgenceDaoImpl(connection);
    Agence agence;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    agence = (Agence) dao.findById(1);
    agence.setNbEmployes(agence.getNbEmployes() + 1);
    testCreate(agence);
    System.out.println("**** update ****");
    List<Entity> agences = (ArrayList<Entity>) dao.findAll();
    agence = (Agence) dao.findById(((Agence) agences.get(agences.size() - 1)).getIdAgence());
    agence.setNbEmployes(agence.getNbEmployes() - 1);
    testUpdate(agence);
    System.out.println("**** delete ****");
    testDelete(agence);
  }

  public static void main(String[] args) {
    try {
      new AgenceJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
