package test.all;

import dao.exception.DaoException;
import dao.jdbc.CategorieDaoImpl;
import model.entity.Categorie;
import model.entity.Entity;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class CategorieJdncDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Categorie c = (Categorie) e;
        System.out.println(c.getIdCategorie() + " | " + c.getLibelleCategorie());
      });
    } catch (DaoException e){
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Categorie categorie = (Categorie) dao.findById(id);
      System.out.println(categorie.getIdCategorie()+ " | " + categorie.getLibelleCategorie());
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try{
      dao.create(entity);
    } catch (DaoException e){
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e){
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try{
      dao.delete(entity);
    } catch (DaoException e){
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Categorie *****");
    dao = new CategorieDaoImpl(connection);
    Categorie categorie;
    System.out.println("***** findAll *****");
    testFindAll();
    System.out.println("***** findById *****");
    testFindById(1);
    System.out.println("***** create *****");
        categorie = (Categorie) dao.findById(1);
    categorie.setLibelleCategorie(categorie.getLibelleCategorie() + '2');
    testCreate(categorie);
    System.out.println("**** update ****");
    List<Entity> categories = (ArrayList<Entity>) dao.findAll();
    categorie = (Categorie) dao.findById(((Categorie) categories.get(categories.size() - 1)).getIdCategorie());
    categorie.setLibelleCategorie(categorie.getLibelleCategorie() + '\b');
    testUpdate(categorie);
    System.out.println("**** delete ****");
    testDelete(categorie);
  }

  public static void main(String[] args) {
    try {
      new CategorieJdncDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
