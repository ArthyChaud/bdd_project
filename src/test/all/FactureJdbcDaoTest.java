package test.all;

import dao.exception.DaoException;
import dao.jdbc.FactureDaoImpl;
import model.entity.Entity;
import model.entity.Facture;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class FactureJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Facture f = (Facture) e;
        System.out.println(f.getIdFacture() + " | " + f.getMontant() + " | " + f.getContrat().getIdContrat());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Facture facture = (Facture) dao.findById(id);
      System.out.println(
          facture.getIdFacture() + " | " + facture.getMontant() + " | " + facture.getContrat().getIdContrat()
      );
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Facture *****");
    dao = new FactureDaoImpl(connection);
    Facture facture;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    facture = (Facture) dao.findById(1);
    facture.setMontant(facture.getMontant() + 1);
    testCreate(facture);
    System.out.println("**** update ****");
    List<Entity> factures = (ArrayList<Entity>) dao.findAll();
    facture = (Facture) dao.findById(((Facture) factures.get(factures.size() - 1)).getIdFacture());
    facture.setMontant(facture.getMontant() - 1);
    testUpdate(facture);
    System.out.println("**** delete ****");
    testDelete(facture);
  }

  public static void main(String[] args) {
    try {
      new FactureJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
