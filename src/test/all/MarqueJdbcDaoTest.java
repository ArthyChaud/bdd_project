package test.all;

import dao.exception.DaoException;
import dao.jdbc.MarqueDaoImpl;
import model.entity.Entity;
import model.entity.Marque;
import test.BasicJdbcDaoTest;

import java.util.ArrayList;
import java.util.List;

public class MarqueJdbcDaoTest extends BasicJdbcDaoTest {
  @Override
  protected void testFindAll() {
    try {
      dao.findAll().forEach(e -> {
        Marque m = (Marque) e;
        System.out.println(m.getIdMarque() + " | " + m.getNomMarque());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testFindById(int id) {
    try {
      Marque marque = (Marque) dao.findById(id);
      System.out.println(marque.getIdMarque() + " | " + marque.getNomMarque());
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testCreate(Entity entity) {
    try {
      dao.create(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testUpdate(Entity entity) {
    try {
      dao.update(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void testDelete(Entity entity) {
    try {
      dao.delete(entity);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void test() throws DaoException {
    System.out.println("***** Marque *****");
    dao = new MarqueDaoImpl(connection);
    Marque marque;
    System.out.println("**** findAll ****");
    testFindAll();
    System.out.println("**** findById ****");
    testFindById(1);
    System.out.println("**** create ****");
    marque = (Marque) dao.findById(1);
    marque.setNomMarque(marque.getNomMarque() + '2');
    testCreate(marque);
    System.out.println("**** update ****");
    List<Entity> marques = (ArrayList<Entity>) dao.findAll();
    marque = (Marque) dao.findById(((Marque) marques.get(marques.size() - 1)).getIdMarque());
    marque.setNomMarque(marque.getNomMarque() + '\b');
    testUpdate(marque);
    System.out.println("**** delete ****");
    testDelete(marque);
  }

  public static void main(String[] args) {
    try {
      new MarqueJdbcDaoTest().test();
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
}
