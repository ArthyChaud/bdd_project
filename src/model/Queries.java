package model;

import dao.exception.DaoException;
import dao.jdbc.*;
import model.entity.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

public class Queries {
  private final Connection connection;

  public Queries(Connection connection) {
    this.connection = connection;
  }

  /* 2 */
  public Contrat rent(Client client, Date date, Date duration, Agence depart, Agence arrival) {
    List<Vehicule> rentedVehicules = new ArrayList<>();
    try {
      new ContratDaoImpl(connection).findAll().forEach(e -> rentedVehicules.add(((Contrat) e).getVehicule()));
      Vehicule vehicule = (Vehicule) new VehiculeDaoImpl(connection)
          .findAll()
          .stream()
          .filter(e ->((Vehicule) e).getAgence().equals(depart))
          .filter(e -> !rentedVehicules.contains(e))
          .collect(Collectors.toList())
          .get(0);
      JdbcDao contratDaoImpl = new ContratDaoImpl(connection);
      contratDaoImpl.create(new Contrat(
          date,
          new Date(date.getTime() + duration.getTime() - new Date(0, 0, 0).getTime()),
          vehicule.getNbKilometres(),
          -1,
          client,
          vehicule,
          arrival
      ));
      List<Entity> contrats = (List<Entity>) contratDaoImpl.findAll();
      return (Contrat) contrats.get(contrats.size() - 1);
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return null;
  }
  /* 3 */
  public void returnVehicule(Contrat contrat) {
    contrat.setKmRetour(contrat.getVehicule().getNbKilometres() - contrat.getKmRetrait());
    try {
      new ContratDaoImpl(connection).update(contrat);
    } catch (DaoException e) {
      e.printStackTrace();
    }
  }
  /* 4 */
  public Facture newFacture(Contrat contrat) {
    JdbcDao factureDaoImpl = new FactureDaoImpl(connection);
    try {
      factureDaoImpl.create(new Facture(
          contrat.getVehicule().getPrixParJourDeLocation()
              * (contrat.getDateDeRetour().getTime() - contrat.getDateDeRetrait().getTime())
              / 86400000,
          contrat
      ));
      List<Entity> factures = (List<Entity>) factureDaoImpl.findAll();
      return (Facture) factures.get(factures.size() - 1);
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return null;
  }
  /* 5 */
  public double getTurnover(Agence agence, int month) {
    try {
      return new FactureDaoImpl(connection)
          .findAll()
          .stream()
          .map(e -> (Facture) e)
          .filter(f -> f.getContrat().getDateDeRetour().getMonth() == month)
          .filter(f -> f.getContrat().getAgenceDeRetour().equals(agence))
          .mapToDouble(Facture::getMontant)
          .sum();
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return 0.0;
  }
  /* 6 */
  public Map<Marque, Integer> howManyVehiculesByBrand() {
    Map<Marque, Integer> map = new HashMap<>();
    try {
      new VehiculeDaoImpl(connection).findAll().stream().map(e -> ((Vehicule) e).getMarque()).forEach(m -> {
        if (map.containsKey(m))
          map.replace(m, map.get(m) + 1);
        else
          map.put(m, 1);
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return map;
  }
  /* 7 */
  public Client theOneWhoHasMadeTheMostRents(Agence agence, int year) {
    Map<Client, Integer> map = new HashMap<>();
    var ref = new Object() {
      Map.Entry<Client, Integer> max = new AbstractMap.SimpleEntry<>(null, 0);
    };
    try {
      new ContratDaoImpl(connection)
          .findAll()
          .stream()
          .map(e -> (Contrat) e)
          .filter(c -> c.getAgenceDeRetour().equals(agence) || c.getVehicule().getAgence().equals(agence))
          .filter(c -> {
            int y = year - 1900;
            return c.getDateDeRetrait().getYear() == y || c.getDateDeRetour().getYear() == y;
          }).map(Contrat::getClient)
          .forEach(c -> {
            if (map.containsKey(c))
              map.replace(c, map.get(c) + 1);
            else
              map.put(c, 1);
          });
      map.forEach((k, v) -> {
        if (ref.max.getValue() < v)
          ref.max = new AbstractMap.SimpleEntry<>(k, v);
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return ref.max.getKey();
  }
  /* 8 */
  public Map<Categorie, Double> turnoverByCategories() {
    Map<Categorie, Double> map = new HashMap<>();
    try {
      new FactureDaoImpl(connection).findAll().stream().map(e -> (Facture) e).forEach(f -> {
        Categorie c = f.getContrat().getVehicule().getCategorie();
        if (map.containsKey(c))
          map.replace(c, map.get(c) + f.getMontant());
        else
          map.put(c, f.getMontant());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return map;
  }
  /* 9 */
  public Map<Type, Double> turnoverByTypes() {
    Map<Type, Double> map = new HashMap<>();
    try {
      new FactureDaoImpl(connection).findAll().stream().map(e -> (Facture) e).forEach(f -> {
        Type t = f.getContrat().getVehicule().getType();
        if (map.containsKey(t))
          map.replace(t, map.get(t) + f.getMontant());
        else
          map.put(t, f.getMontant());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return map;
  }
  /* 10 */
  public Map<Agence, Integer> nbOldVehiculeByAgence() {
    Map<Agence, Integer> map = new HashMap<>();
    try {
      new AgenceDaoImpl(connection).findAll().forEach(e -> map.put(((Agence) e), 0));
      new VehiculeDaoImpl(connection)
          .findAll()
          .stream()
          .map(e -> (Vehicule) e)
          .filter(v -> {
            java.util.Date date = Calendar.getInstance().getTime();
            return v
                .getDateMiseEnCirculation()
                .compareTo(
                    new Date(date.getYear() - 2, date.getMonth(), date.getDay())
                ) <= 0;
          }).filter(v -> v.getNbKilometres() > 150000)
          .map(Vehicule::getAgence)
          .forEach(a -> map.replace(a, map.get(a) + 1));
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return map;
  }
  /* 11 */
  public Map<Agence, Double> turnoverByAgence() {
    Map<Agence, Double> map = new HashMap<>();
    try {
      new AgenceDaoImpl(connection).findAll().forEach(e -> map.put(((Agence) e), 0.0));
      new FactureDaoImpl(connection).findAll().stream().map(e -> (Facture) e).forEach(f -> {
        Agence a = f.getContrat().getAgenceDeRetour();
        map.replace(a, map.get(a) + f.getMontant());
      });
    } catch (DaoException e) {
      e.printStackTrace();
    }
    return map;
  }
}
