package model.entity;

import java.sql.Date;
import java.util.Objects;

public class Vehicule implements Entity {
  private String immatriculation;
  private Date dateMiseEnCirculation;
  private String etat;
  private int nbKilometres;
  private double prixParJourDeLocation;
  private Marque marque;
  private Modele modele;
  private Categorie categorie;
  private Type type;
  private Agence agence;

  public Vehicule() {}

  public Vehicule(
      Date dateMiseEnCirculation,
      String etat,
      int nbKilometres,
      double prixParJourDeLocation,
      Marque marque,
      Modele modele,
      Categorie categorie,
      Type type,
      Agence agence
  ) {
    this(
        "",
        dateMiseEnCirculation,
        etat,
        nbKilometres,
        prixParJourDeLocation,
        marque,
        modele,
        categorie,
        type,
        agence
    );
  }

  public Vehicule(
      String immatriculation,
      Date dateMiseEnCirculation,
      String etat,
      int nbKilometres,
      double prixParJourDeLocation,
      Marque marque,
      Modele modele,
      Categorie categorie,
      Type type,
      Agence agence
  ) {
    this.immatriculation = immatriculation;
    this.dateMiseEnCirculation = dateMiseEnCirculation;
    this.etat = etat;
    this.nbKilometres = nbKilometres;
    this.prixParJourDeLocation = prixParJourDeLocation;
    this.marque = marque;
    this.modele = modele;
    this.categorie = categorie;
    this.type = type;
    this.agence = agence;
  }

  public String getImmatriculation() {
    return immatriculation;
  }

  public void setImmatriculation(String immatriculation) {
    this.immatriculation = immatriculation;
  }

  public Date getDateMiseEnCirculation() {
    return dateMiseEnCirculation;
  }

  public void setDateMiseEnCirculation(Date dateMiseEnCirculation) {
    this.dateMiseEnCirculation = dateMiseEnCirculation;
  }

  public String getEtat() {
    return etat;
  }

  public void setEtat(String etat) {
    this.etat = etat;
  }

  public int getNbKilometres() {
    return nbKilometres;
  }

  public void setNbKilometres(int nbKilometres) {
    this.nbKilometres = nbKilometres;
  }

  public double getPrixParJourDeLocation() {
    return prixParJourDeLocation;
  }

  public void setPrixParJourDeLocation(double prixParJourDeLocation) {
    this.prixParJourDeLocation = prixParJourDeLocation;
  }

  public Marque getMarque() {
    return marque;
  }

  public void setMarque(Marque marque) {
    this.marque = marque;
  }

  public Modele getModele() {
    return modele;
  }

  public void setModele(Modele modele) {
    this.modele = modele;
  }

  public Categorie getCategorie() {
    return categorie;
  }

  public void setCategorie(Categorie categorie) {
    this.categorie = categorie;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Agence getAgence() {
    return agence;
  }

  public void setAgence(Agence agence) {
    this.agence = agence;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof  Vehicule))
      return false;
    return immatriculation.equals(((Vehicule) o).immatriculation);
  }

  @Override
  public int hashCode() {
    return immatriculation.hashCode();
  }

  @Override
  public String toString() {
    return "Vehicule{" +
        "immatriculation='" + immatriculation + '\'' +
        ", dateMiseEnCirculation=" + dateMiseEnCirculation +
        ", etat='" + etat + '\'' +
        ", nbKilometres=" + nbKilometres +
        ", prixParJourDeLocation=" + prixParJourDeLocation +
        ", marque=" + marque +
        ", modele=" + modele +
        ", categorie=" + categorie +
        ", type=" + type +
        ", agence=" + agence +
        '}';
  }
}
