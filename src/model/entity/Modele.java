package model.entity;

import java.util.Objects;

public class Modele implements Entity {
  private int idModele;
  private String denomination;
  private int puissanceFiscale;

  public Modele() {}

  public Modele(String denomination, int puissanceFiscale) {
    this(-1, denomination, puissanceFiscale);
  }

  public Modele(int idModele, String denomination, int puissanceFiscale) {
    this.idModele = idModele;
    this.denomination = denomination;
    this.puissanceFiscale = puissanceFiscale;
  }

  public int getIdModele() {
    return idModele;
  }

  public void setIdModele(int idModele) {
    this.idModele = idModele;
  }

  public String getDenomination() {
    return denomination;
  }

  public void setDenomination(String denomination) {
    this.denomination = denomination;
  }

  public int getPuissanceFiscale() {
    return puissanceFiscale;
  }

  public void setPuissanceFiscale(int puissanceFiscale) {
    this.puissanceFiscale = puissanceFiscale;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Modele))
      return false;
    return idModele == ((Modele) o).idModele;
  }

  @Override
  public int hashCode() {
    return idModele;
  }

  @Override
  public String toString() {
    return "Modele{" +
        "idModele=" + idModele +
        ", denomination='" + denomination + '\'' +
        ", puissanceFiscale=" + puissanceFiscale +
        '}';
  }
}
