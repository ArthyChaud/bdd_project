package model.entity;

import java.util.Objects;

public class Type implements Entity {
  private int idType;
  private String libelleType;

  public Type() {}

  public Type(String libelleType) {
    this(-1, libelleType);
  }

  public Type(int idType, String libelleType) {
    this.idType = idType;
    this.libelleType = libelleType;
  }

  public int getIdType() {
    return idType;
  }

  public void setIdType(int idType) {
    this.idType = idType;
  }

  public String getLibelleType() {
    return libelleType;
  }

  public void setLibelleType(String libelleType) {
    this.libelleType = libelleType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Type))
      return false;
    return idType == ((Type) o).idType;
  }

  @Override
  public int hashCode() {
    return idType;
  }

  @Override
  public String toString() {
    return "Type{" +
        "idType=" + idType +
        ", libelleType='" + libelleType + '\'' +
        '}';
  }
}
