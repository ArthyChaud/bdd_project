package model.entity;

import java.util.Objects;

public class Ville implements Entity {
  private int idVille;
  private String nomVille;
  private int nombreHabitants;

  public Ville() {}

  public Ville(String nomVille, int nombreHabitants) {
    this(-1, nomVille, nombreHabitants);
  }

  public Ville(int idVille, String nomVille, int nombreHabitants) {
    this.idVille = idVille;
    this.nomVille = nomVille;
    this.nombreHabitants = nombreHabitants;
  }

  public int getIdVille() {
    return idVille;
  }

  public void setIdVille(int idVille) {
    this.idVille = idVille;
  }

  public String getNomVille() {
    return nomVille;
  }

  public void setNomVille(String nomVille) {
    this.nomVille = nomVille;
  }

  public int getNombreHabitants() {
    return nombreHabitants;
  }

  public void setNombreHabitants(int nombreHabitants) {
    this.nombreHabitants = nombreHabitants;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Ville))
      return false;
    return idVille == ((Ville) o).idVille;
  }

  @Override
  public int hashCode() {
    return idVille;
  }

  @Override
  public String toString() {
    return "Ville{" +
        "idVille=" + idVille +
        ", nomVille='" + nomVille + '\'' +
        ", nombreHabitants=" + nombreHabitants +
        '}';
  }
}
