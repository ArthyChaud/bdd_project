package model.entity;

import java.util.Objects;

public class Facture implements Entity {
  private int idFacture;
  private double montant;
  private Contrat contrat;

  public Facture() {}

  public Facture(double montant, Contrat contrat) {
    this(-1, montant, contrat);
  }

  public Facture(int idFacture, double montant, Contrat contrat) {
    this.idFacture = idFacture;
    this.montant = montant;
    this.contrat = contrat;
  }

  public int getIdFacture() {
    return idFacture;
  }

  public void setIdFacture(int idFacture) {
    this.idFacture = idFacture;
  }

  public double getMontant() {
    return montant;
  }

  public void setMontant(double montant) {
    this.montant = montant;
  }

  public Contrat getContrat() {
    return contrat;
  }

  public void setContrat(Contrat contrat) {
    this.contrat = contrat;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Facture))
      return false;
    return idFacture == ((Facture) o).idFacture;
  }

  @Override
  public int hashCode() {
    return idFacture;
  }

  @Override
  public String toString() {
    return "Facture{" +
        "idFacture=" + idFacture +
        ", montant=" + montant +
        ", contrat=" + contrat +
        '}';
  }
}
