package model.entity;

public interface Entity {
  @Override
  String toString();
}
