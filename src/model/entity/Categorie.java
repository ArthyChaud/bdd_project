package model.entity;

import java.util.Objects;

public class Categorie implements Entity {
  private int idCategorie;
  private String libelleCategorie;

  public Categorie() {}

  public Categorie(String libelleCategorie) {
    this(-1, libelleCategorie);
  }

  public Categorie(int idCategorie, String libelleCategorie) {
    this.idCategorie = idCategorie;
    this.libelleCategorie = libelleCategorie;
  }

  public int getIdCategorie() {
    return idCategorie;
  }

  public void setIdCategorie(int idCategorie) {
    this.idCategorie = idCategorie;
  }

  public String getLibelleCategorie() {
    return libelleCategorie;
  }

  public void setLibelleCategorie(String libelleCategorie) {
    this.libelleCategorie = libelleCategorie;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Categorie))
      return false;
    return idCategorie == ((Categorie) o).idCategorie;
  }

  @Override
  public int hashCode() {
    return idCategorie;
  }

  @Override
  public String toString() {
    return "Categorie{" +
        "idCategorie=" + idCategorie +
        ", libelleCategorie='" + libelleCategorie + '\'' +
        '}';
  }
}
