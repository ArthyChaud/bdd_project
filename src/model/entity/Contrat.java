package model.entity;

import java.sql.Date;
import java.util.Objects;

public class Contrat implements Entity {
  private int idContrat;
  private Date dateDeRetrait;
  private Date dateDeRetour;
  private int kmRetrait;
  private int kmRetour;
  private Client client;
  private Vehicule vehicule;
  private Agence agenceDeRetour;

  public Contrat() {}

  public Contrat(
      Date dateDeRetrait,
      Date dateDeRetour,
      int kmRetrait,
      int kmRetour,
      Client client,
      Vehicule vehicule,
      Agence agenceDeRetour
  ) {
    this(-1, dateDeRetrait, dateDeRetour, kmRetrait, kmRetour, client, vehicule, agenceDeRetour);
  }

  public Contrat(
      int idContrat,
      Date dateDeRetrait,
      Date dateDeRetour,
      int kmRetrait,
      int kmRetour,
      Client client,
      Vehicule vehicule,
      Agence agenceDeRetour
  ) {
    this.idContrat = idContrat;
    this.dateDeRetrait = dateDeRetrait;
    this.dateDeRetour = dateDeRetour;
    this.kmRetrait = kmRetrait;
    this.kmRetour = kmRetour;
    this.client = client;
    this.vehicule = vehicule;
    this.agenceDeRetour = agenceDeRetour;
  }

  public int getIdContrat() {
    return idContrat;
  }

  public void setIdContrat(int idContrat) {
    this.idContrat = idContrat;
  }

  public Date getDateDeRetrait() {
    return dateDeRetrait;
  }

  public void setDateDeRetrait(Date dateDeRetrait) {
    this.dateDeRetrait = dateDeRetrait;
  }

  public Date getDateDeRetour() {
    return dateDeRetour;
  }

  public void setDateDeRetour(Date dateDeRetour) {
    this.dateDeRetour = dateDeRetour;
  }

  public int getKmRetrait() {
    return kmRetrait;
  }

  public void setKmRetrait(int kmRetrait) {
    this.kmRetrait = kmRetrait;
  }

  public int getKmRetour() {
    return kmRetour;
  }

  public void setKmRetour(int kmRetour) {
    this.kmRetour = kmRetour;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Vehicule getVehicule() {
    return vehicule;
  }

  public void setVehicule(Vehicule vehicule) {
    this.vehicule = vehicule;
  }

  public Agence getAgenceDeRetour() {
    return agenceDeRetour;
  }

  public void setAgenceDeRetour(Agence agenceDeRetour) {
    this.agenceDeRetour = agenceDeRetour;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Contrat))
      return false;
    return idContrat == ((Contrat) o).idContrat;
  }

  @Override
  public int hashCode() {
    return idContrat;
  }

  @Override
  public String toString() {
    return "Contrat{" +
        "idContrat=" + idContrat +
        ", dateDeRetrait=" + dateDeRetrait +
        ", dateDeRetour=" + dateDeRetour +
        ", kmRetrait=" + kmRetrait +
        ", kmRetour=" + kmRetour +
        ", client=" + client +
        ", vehicule=" + vehicule +
        ", agenceDeRetour=" + agenceDeRetour +
        '}';
  }
}
