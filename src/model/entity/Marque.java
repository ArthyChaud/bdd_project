package model.entity;

import java.util.Objects;

public class Marque implements Entity {
  private int idMarque;
  private String nomMarque;

  public Marque() {}

  public Marque(String nomMarque) {
    this(-1, nomMarque);
  }

  public Marque(int idMarque, String nomMarque) {
    this.idMarque = idMarque;
    this.nomMarque = nomMarque;
  }

  public int getIdMarque() {
    return idMarque;
  }

  public void setIdMarque(int idMarque) {
    this.idMarque = idMarque;
  }

  public String getNomMarque() {
    return nomMarque;
  }

  public void setNomMarque(String nomMarque) {
    this.nomMarque = nomMarque;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Marque))
      return false;
    return idMarque == ((Marque) o).idMarque;
  }

  @Override
  public int hashCode() {
    return idMarque;
  }

  @Override
  public String toString() {
    return "Marque{" +
        "idMarque=" + idMarque +
        ", nomMarque='" + nomMarque + '\'' +
        '}';
  }
}
