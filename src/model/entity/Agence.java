package model.entity;

import java.util.Objects;

public class Agence implements Entity {
  private int idAgence;
  private int nbEmployes;
  private Ville ville;

  public Agence() {}

  public Agence(int nbEmployes, Ville ville) {
    this(-1, nbEmployes, ville);
  }

  public Agence(int idAgence, int nbEmployes, Ville ville) {
    this.idAgence = idAgence;
    this.nbEmployes = nbEmployes;
    this.ville = ville;
  }

  public int getIdAgence() {
    return idAgence;
  }

  public void setIdAgence(int idAgence) {
    this.idAgence = idAgence;
  }

  public int getNbEmployes() {
    return nbEmployes;
  }

  public void setNbEmployes(int nbEmployes) {
    this.nbEmployes = nbEmployes;
  }

  public Ville getVille() {
    return ville;
  }

  public void setVille(Ville ville) {
    this.ville = ville;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Agence))
      return false;
    return idAgence == ((Agence) o).idAgence;
  }

  @Override
  public int hashCode() {
    return idAgence;
  }

  @Override
  public String toString() {
    return "Agence{" +
        "idAgence=" + idAgence +
        ", nbEmployes=" + nbEmployes +
        ", ville=" + ville +
        '}';
  }
}
