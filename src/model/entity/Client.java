package model.entity;

import java.util.Objects;

public class Client implements Entity {
  private int idClient;
  private String nomClient;
  private String adresseClient;
  private int codePostalClient;
  private Ville ville;

  public Client() {}

  public Client(String nomClient, String adresseClient, int codePostalClient, Ville ville) {
    this(-1, nomClient, adresseClient, codePostalClient, ville);
  }

  public Client(int idClient, String nomClient, String adresseClient, int codePostalClient, Ville ville) {
    this.idClient = idClient;
    this.nomClient = nomClient;
    this.adresseClient = adresseClient;
    this.codePostalClient = codePostalClient;
    this.ville = ville;
  }

  public int getIdClient() {
    return idClient;
  }

  public void setIdClient(int idClient) {
    this.idClient = idClient;
  }

  public String getNomClient() {
    return nomClient;
  }

  public void setNomClient(String nomClient) {
    this.nomClient = nomClient;
  }

  public String getAdresseClient() {
    return adresseClient;
  }

  public void setAdresseClient(String adresseClient) {
    this.adresseClient = adresseClient;
  }

  public int getCodePostalClient() {
    return codePostalClient;
  }

  public void setCodePostalClient(int codePostalClient) {
    this.codePostalClient = codePostalClient;
  }

  public Ville getVille() {
    return ville;
  }

  public void setVille(Ville ville) {
    this.ville = ville;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Client))
      return false;
    return idClient == ((Client) o).idClient;
  }

  @Override
  public int hashCode() {
    return idClient;
  }

  @Override
  public String toString() {
    return "Client{" +
        "idClient=" + idClient +
        ", nomClient='" + nomClient + '\'' +
        ", adresseClient='" + adresseClient + '\'' +
        ", codePostalClient=" + codePostalClient +
        ", ville=" + ville +
        '}';
  }
}
