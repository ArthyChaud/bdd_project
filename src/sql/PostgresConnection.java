package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresConnection {

  private static String serveur = "localhost";
  private static String baseName = "projet";
  private static String user = "arthychaud";
  private static String password = Password.password;

  private static Connection connection;

  public static Connection getInstance() {

    if (connection == null) {
      String url = "jdbc:postgresql://" + serveur + "/" + baseName;
      try {
        connection = DriverManager.getConnection(url, user, password );
      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }
    }
    return connection;
  }
}