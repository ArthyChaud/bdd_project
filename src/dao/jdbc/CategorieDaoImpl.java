package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Categorie;
import model.entity.Entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CategorieDaoImpl extends JdbcDao {
  public CategorieDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> categories = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM categorie");
      while (resultSet.next())
        categories.add(new Categorie(
            resultSet.getInt("idcategorie"),
            resultSet.getString("libellecategorie")
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return categories;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM categorie WHERE idcategorie = ?");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Categorie(
          resultSet.getInt("idcategorie"),
          resultSet.getString("libellecategorie")
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Categorie categorie = (Categorie) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO categorie(libellecategorie) VALUES (?)"
      );
      statement.setString(1, categorie.getLibelleCategorie());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE categorie SET libellecategorie = ? WHERE idcategorie = ?"
      );
      Categorie categorie = (Categorie) entity;
      statement.setString(1, categorie.getLibelleCategorie());
      statement.setInt(2, categorie.getIdCategorie());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM categorie WHERE idcategorie = ?");
      statement.setInt(1, ((Categorie) entity).getIdCategorie());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public Map<String, Double> turnoverByCategories() throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          """
            SELECT ca.libellecategorie, SUM(f.montant)
            FROM facture f
            INNER JOIN contrat co
              ON co.idcontrat = f.idcontrat
            INNER JOIN vehicule v
              ON co.immatriculation = v.immatriculation
            INNER JOIN categorie ca
              ON v.idcategorie = ca.idcategorie
            GROUP BY ca.idcategorie
            """
      );
      ResultSet resultSet = statement.executeQuery();
      Map<String, Double> map = new HashMap<>();
      while (resultSet.next())
        map.put(resultSet.getString(1), resultSet.getDouble(2));
      return map;
    } catch (SQLException e) {
      throw new  DaoException(e);
    }
  }
}
