package dao.jdbc;

import dao.exception.DaoException;
import model.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class VehiculeDaoImpl extends JdbcDao {
  public VehiculeDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> vehicules = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM vehicule");
      while (resultSet.next())
        vehicules.add(new Vehicule(
            resultSet.getString("immatriculation"),
            resultSet.getDate("datemiseencirculation"),
            resultSet.getString("etat"),
            resultSet.getInt("nbkilometres"),
            resultSet.getDouble("prixparjourdelocation"),
            (Marque) new MarqueDaoImpl(connection).findById(resultSet.getInt("idmarque")),
            (Modele) new ModeleDaoImpl(connection).findById(resultSet.getInt("idmodele")),
            (Categorie) new CategorieDaoImpl(connection).findById(resultSet.getInt("idcategorie")),
            (Type) new TypeDaoImpl(connection).findById(resultSet.getInt("idtype")),
            (Agence) new AgenceDaoImpl(connection).findById(resultSet.getInt("idagence"))
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return vehicules;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    return findById(Integer.toString(id));
  }

  public Entity findById(String id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "SELECT * FROM vehicule WHERE immatriculation = ? LIMIT 1"
      );
      statement.setString(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Vehicule(
          resultSet.getString("immatriculation"),
          resultSet.getDate("datemiseencirculation"),
          resultSet.getString("etat"),
          resultSet.getInt("nbkilometres"),
          resultSet.getDouble("prixparjourdelocation"),
          (Marque) new MarqueDaoImpl(connection).findById(resultSet.getInt("idmarque")),
          (Modele) new ModeleDaoImpl(connection).findById(resultSet.getInt("idmodele")),
          (Categorie) new CategorieDaoImpl(connection).findById(resultSet.getInt("idcategorie")),
          (Type) new TypeDaoImpl(connection).findById(resultSet.getInt("idtype")),
          (Agence) new AgenceDaoImpl(connection).findById(resultSet.getInt("idagence"))
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Vehicule vehicule = (Vehicule) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO vehicule(" +
              "immatriculation, datemiseencirculation, etat, nbkilometres, prixparjourdelocation, " +
              "idmarque, idmodele, idcategorie, idtype, idagence" +
          ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
      );
      statement.setString(1, vehicule.getImmatriculation());
      statement.setDate(2, vehicule.getDateMiseEnCirculation());
      statement.setString(3, vehicule.getEtat());
      statement.setInt(4, vehicule.getNbKilometres());
      statement.setDouble(5, vehicule.getPrixParJourDeLocation());
      statement.setInt(6, vehicule.getMarque().getIdMarque());
      statement.setInt(7, vehicule.getModele().getIdModele());
      statement.setInt(8, vehicule.getCategorie().getIdCategorie());
      statement.setInt(9, vehicule.getType().getIdType());
      statement.setInt(10, vehicule.getAgence().getIdAgence());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE vehicule SET " +
              "datemiseencirculation = ?, etat = ?, nbkilometres = ?, prixparjourdelocation = ?, " +
              "idmarque = ?, idmodele = ?, idcategorie = ?, idtype = ?, idagence = ? " +
          "WHERE immatriculation = ?"
      );
      Vehicule vehicule = (Vehicule) entity;
      statement.setDate(1, vehicule.getDateMiseEnCirculation());
      statement.setString(2, vehicule.getEtat());
      statement.setInt(3, vehicule.getNbKilometres());
      statement.setDouble(4, vehicule.getPrixParJourDeLocation());
      statement.setInt(5, vehicule.getMarque().getIdMarque());
      statement.setInt(6, vehicule.getModele().getIdModele());
      statement.setInt(7, vehicule.getCategorie().getIdCategorie());
      statement.setInt(8, vehicule.getType().getIdType());
      statement.setInt(9, vehicule.getAgence().getIdAgence());
      statement.setString(10, vehicule.getImmatriculation());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM vehicule WHERE immatriculation = ?");
      statement.setString(1, ((Vehicule) entity).getImmatriculation());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
