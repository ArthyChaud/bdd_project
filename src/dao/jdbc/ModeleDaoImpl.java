package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Entity;
import model.entity.Modele;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class ModeleDaoImpl extends JdbcDao {
  public ModeleDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> modeles = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM modele");
      while (resultSet.next())
        modeles.add(new Modele(
           resultSet.getInt("idmodele"),
           resultSet.getString("denomination"),
           resultSet.getInt("puissancefiscale")
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return modeles;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM modele WHERE idmodele = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Modele(
          resultSet.getInt("idmodele"),
          resultSet.getString("denomination"),
          resultSet.getInt("puissancefiscale")
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Modele modele = (Modele) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO modele(denomination, puissancefiscale) VALUES (?, ?)"
      );
      statement.setString(1, modele.getDenomination());
      statement.setInt(2,modele.getPuissanceFiscale());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE modele SET denomination = ?, puissancefiscale = ? WHERE idmodele = ?"
      );
      Modele modele = (Modele) entity;
      statement.setString(1, modele.getDenomination());
      statement.setInt(2, modele.getPuissanceFiscale());
      statement.setInt(3, modele.getIdModele());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM modele WHERE idmodele = ?");
      statement.setInt(1, ((Modele) entity).getIdModele());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
