package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Contrat;
import model.entity.Entity;
import model.entity.Facture;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class FactureDaoImpl extends JdbcDao {
  public FactureDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> factures = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM facture");
      while (resultSet.next())
        factures.add(new Facture(
            resultSet.getInt("idfacture"),
            resultSet.getDouble("montant"),
            (Contrat) new ContratDaoImpl(connection).findById(resultSet.getInt("idcontrat"))
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return factures;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM facture WHERE idfacture = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Facture(
          resultSet.getInt("idfacture"),
          resultSet.getDouble("montant"),
          (Contrat) new ContratDaoImpl(connection).findById(resultSet.getInt("idcontrat"))
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Facture facture = (Facture) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO facture(montant, idcontrat) VALUES (?, ?)"
      );
      statement.setDouble(1, facture.getMontant());
      statement.setInt(2, facture.getContrat().getIdContrat());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE facture SET montant = ?, idcontrat = ? WHERE idfacture = ?"
      );
      Facture facture = (Facture) entity;
      statement.setDouble(1, facture.getMontant());
      statement.setInt(2, facture.getContrat().getIdContrat());
      statement.setInt(3, facture.getIdFacture());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM facture WHERE idfacture = ?");
      statement.setInt(1, ((Facture) entity).getIdFacture());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public void newFacture(Contrat contrat) throws DaoException {
    create(new Facture(
        contrat.getVehicule().getPrixParJourDeLocation()
            * (contrat.getDateDeRetour().getTime() - contrat.getDateDeRetrait().getTime())
            / 86400000,
        contrat
    ));
  }
}
