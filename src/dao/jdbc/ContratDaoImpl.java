package dao.jdbc;

import dao.exception.DaoException;
import model.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class ContratDaoImpl extends JdbcDao {
  public ContratDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> contrats = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM contrat");
      while (resultSet.next())
        contrats.add(new Contrat(
            resultSet.getInt("idcontrat"),
            resultSet.getDate("datederetrait"),
            resultSet.getDate("datederetour"),
            resultSet.getInt("kmretrait"),
            resultSet.getInt("kmretour"),
            (Client) new ClientDaoImpl(connection).findById(resultSet.getInt("idclient")),
            (Vehicule) new VehiculeDaoImpl(connection).findById(resultSet.getString("immatriculation")),
            (Agence) new AgenceDaoImpl(connection).findById(resultSet.getInt("idagencederetour"))
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return contrats;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM contrat WHERE idcontrat = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Contrat(
          resultSet.getInt("idcontrat"),
          resultSet.getDate("datederetrait"),
          resultSet.getDate("datederetour"),
          resultSet.getInt("kmretrait"),
          resultSet.getInt("kmretour"),
          (Client) new ClientDaoImpl(connection).findById(resultSet.getInt("idclient")),
          (Vehicule) new VehiculeDaoImpl(connection).findById(resultSet.getString("immatriculation")),
          (Agence) new AgenceDaoImpl(connection).findById(resultSet.getInt("idagencederetour"))
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Contrat contrat = (Contrat) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO contrat(" +
              "datederetrait, datederetour, kmretrait, kmretour, idclient, immatriculation, idagencederetour" +
          ") VALUES (?, ?, ?, ?, ?, ?, ?)"
      );
      statement.setDate(1, contrat.getDateDeRetrait());
      statement.setDate(2, contrat.getDateDeRetour());
      statement.setInt(3, contrat.getKmRetrait());
      statement.setInt(4, contrat.getKmRetour());
      statement.setInt(5, contrat.getClient().getIdClient());
      statement.setString(6, contrat.getVehicule().getImmatriculation());
      statement.setInt(7, contrat.getAgenceDeRetour().getIdAgence());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE contrat SET " +
              "datederetrait = ?, datederetour = ?, kmretrait = ?, kmretour = ?, " +
              "idclient = ?, immatriculation = ?, idagencederetour = ?" +
          "WHERE idcontrat = ?"
      );
      Contrat contrat = (Contrat) entity;
      statement.setDate(1, contrat.getDateDeRetrait());
      statement.setDate(2, contrat.getDateDeRetour());
      statement.setInt(3, contrat.getKmRetrait());
      statement.setInt(4, contrat.getKmRetour());
      statement.setInt(5, contrat.getClient().getIdClient());
      statement.setString(6, contrat.getVehicule().getImmatriculation());
      statement.setInt(7, contrat.getAgenceDeRetour().getIdAgence());
      statement.setInt(8, contrat.getIdContrat());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM contrat WHERE idcontrat = ?");
      statement.setInt(1, ((Contrat) entity).getIdContrat());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public void rent(Client client, Date date, Date duration, Vehicule vehicule, Agence depart, Agence arrival) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO contrat VALUES (" +
              "default, ?, ?, (SELECT nbkilometres FROM vehicule WHERE immatriculation = ?), -1, ?, ?, ?" +
          ")"
      );
      statement.setDate(1, date);
      statement.setDate(
          2, new Date(date.getTime() + duration.getTime() - new Date(0, 0, 0).getTime())
      );
      statement.setString(3, vehicule.getImmatriculation());
      statement.setInt(4, client.getIdClient());
      statement.setString(5, vehicule.getImmatriculation());
      statement.setInt(6, arrival.getIdAgence());
      statement.executeUpdate();
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public void returnVehicule(Contrat contrat) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE contrat " +
          "SET kmretour = (" +
            "SELECT nbkilometres FROM vehicule WHERE immatriculation = ?" +
          ") WHERE idcontrat = ?"
      );
      statement.setString(1, contrat.getVehicule().getImmatriculation());
      statement.setInt(2, contrat.getIdContrat());
      statement.executeUpdate();
    } catch (SQLException e) {
      throw new DaoException(e);
    }

  }
}
