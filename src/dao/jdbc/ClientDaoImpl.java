package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Agence;
import model.entity.Client;
import model.entity.Entity;
import model.entity.Ville;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class ClientDaoImpl extends JdbcDao {
  public ClientDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> clients = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM client");
      while (resultSet.next())
        clients.add(new Client(
            resultSet.getInt("idclient"),
            resultSet.getString("nomclient"),
            resultSet.getString("adresseclient"),
            resultSet.getInt("codepostalclient"),
            (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt("idville"))
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return clients;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM client WHERE idclient = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Client(
          resultSet.getInt("idclient"),
          resultSet.getString("nomclient"),
          resultSet.getString("adresseclient"),
          resultSet.getInt("codepostalclient"),
          (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt("idville"))
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Client client = (Client) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO client(nomclient, adresseclient, codepostalclient, idville) values (?, ?, ?, ?)"
      );
      statement.setString(1, client.getNomClient());
      statement.setString(2, client.getAdresseClient());
      statement.setInt(3, client.getCodePostalClient());
      statement.setInt(4, client.getVille().getIdVille());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE client SET nomclient = ?, adresseclient = ?, codepostalclient = ?, idville = ?" +
              "WHERE idclient = ?"
      );
      Client client = (Client) entity;
      statement.setString(1, client.getNomClient());
      statement.setString(2, client.getAdresseClient());
      statement.setInt(3, client.getCodePostalClient());
      statement.setInt(4, client.getVille().getIdVille());
      statement.setInt(5, client.getIdClient());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM client WHERE idclient = ?");
      statement.setInt(1, ((Client) entity).getIdClient());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public Client theOneWhoHasMadeTheMostRents(Agence agence, int year) throws DaoException {
    Date
        dateMin = new Date(year - 1900 - 1, Calendar.DECEMBER, 31),
        dateMax = new Date(year - 1900 + 1, Calendar.JANUARY, 1);
    try {
      PreparedStatement statement = connection.prepareStatement(
          """
          SELECT cl.*, SUM(f.montant) AS total
          FROM contrat co
          INNER JOIN client cl
            ON cl.idclient = co.idclient
          INNER JOIN facture f
            ON f.idcontrat = co.idcontrat
          WHERE datederetrait > ? AND datederetrait < ?
            AND co.idagencederetour = ?
          GROUP BY cl.idclient
          ORDER BY total DESC
          LIMIT 1
          """
      );
      statement.setDate(1, dateMin);
      statement.setDate(2, dateMax);
      statement.setInt(3, agence.getIdAgence());
      ResultSet resultSet = statement.executeQuery();
      if (resultSet.next())
        return new Client(
            resultSet.getInt(1),
            resultSet.getString(2),
            resultSet.getString(3),
            resultSet.getInt(4),
            (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt(5))
        );
      return null;
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
