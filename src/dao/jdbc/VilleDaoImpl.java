package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Entity;
import model.entity.Ville;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class VilleDaoImpl extends JdbcDao {
  public VilleDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> villes = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM ville");
      while (resultSet.next())
        villes.add(new Ville(
            resultSet.getInt("idville"),
            resultSet.getString("nomville"),
            resultSet.getInt("nombrehabitants")
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return villes;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM ville WHERE idville = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Ville(
          resultSet.getInt("idville"), resultSet.getString("nomville"), resultSet.getInt("nombrehabitants")
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Ville ville = (Ville) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO ville(nomville, nombrehabitants) values (?, ?)"
      );
      statement.setString(1, ville.getNomVille());
      statement.setInt(2, ville.getNombreHabitants());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE ville SET nomville = ?, nombrehabitants = ? WHERE idville = ?"
      );
      Ville ville = (Ville) entity;
      statement.setString(1, ville.getNomVille());
      statement.setInt(2, ville.getNombreHabitants());
      statement.setInt(3, ville.getIdVille());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM ville WHERE idville = ?");
      statement.setInt(1, ((Ville) entity).getIdVille());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
