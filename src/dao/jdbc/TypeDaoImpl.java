package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Entity;
import model.entity.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TypeDaoImpl extends JdbcDao {
  public TypeDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> types = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM type");
      while (resultSet.next())
        types.add(new Type(
            resultSet.getInt("idtype"),
            resultSet.getString("libelletype")
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return types;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM type WHERE idtype = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Type(
          resultSet.getInt("idtype"),
          resultSet.getString("libelletype")
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Type type = (Type) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO type(libelletype) VALUES (?)"
      );
      statement.setString(1, type.getLibelleType());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE type SET libelletype = ? WHERE idtype = ?"
      );
      Type type = (Type) entity;
      statement.setString(1, type.getLibelleType());
      statement.setInt(2, type.getIdType());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM type WHERE idtype = ?");
      statement.setInt(1, ((Type) entity).getIdType());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public Map<String, Double> turnoverByTypes() throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          """
            SELECT t.libelletype, SUM(f.montant)
            FROM facture f
            INNER JOIN contrat c
              ON c.idcontrat = f.idcontrat
            INNER JOIN vehicule v
              ON c.immatriculation = v.immatriculation
            INNER JOIN type t
              ON v.idtype = t.idtype
            GROUP BY t.idtype
            """
      );
      ResultSet resultSet = statement.executeQuery();
      Map<String, Double> map = new HashMap<>();
      while (resultSet.next())
        map.put(resultSet.getString(1), resultSet.getDouble(2));
      return map;
    } catch (SQLException e) {
      throw new  DaoException(e);
    }
  }
}
