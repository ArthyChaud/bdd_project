package dao.jdbc;

import dao.exception.DaoException;
import model.entity.Entity;
import model.entity.Marque;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MarqueDaoImpl extends JdbcDao {
  public MarqueDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> marques = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM marque");
      while (resultSet.next())
        marques.add(new Marque(
            resultSet.getInt("idmarque"),
            resultSet.getString("nomMarque")
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return marques;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM marque WHERE idmarque = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet = statement.executeQuery();
      resultSet.next();
      return new Marque(
          resultSet.getInt("idmarque"),
          resultSet.getString("nomMarque")
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Marque marque = (Marque) entity;
    try {
      PreparedStatement statement = connection.prepareStatement("INSERT INTO marque(nommarque) VALUES (?)");
      statement.setString(1, marque.getNomMarque());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE marque SET nommarque = ? WHERE idmarque = ?"
      );
      Marque marque = (Marque) entity;
      statement.setString(1, marque.getNomMarque());
      statement.setInt(2, marque.getIdMarque());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM marque WHERE idmarque = ?");
      statement.setInt(1, ((Marque) entity).getIdMarque());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public Map<String, Integer> howManyVehiculesByBrand() throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          """
          SELECT m.nommarque, COUNT(v.immatriculation)
          FROM vehicule v
          INNER JOIN marque m
            ON v.idmarque = m.idmarque
          GROUP BY m.idmarque
          """
      );
      ResultSet resultSet = statement.executeQuery();
      Map<String, Integer> map = new HashMap<>();
      while (resultSet.next())
        map.put(resultSet.getString(1), resultSet.getInt(2));
      return map;
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
