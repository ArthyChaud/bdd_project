package dao.jdbc;

import dao.exception.DaoException;
import model.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class AgenceDaoImpl extends JdbcDao {
  public AgenceDaoImpl(Connection connection) {
    super(connection);
  }

  @Override
  public Collection<Entity> findAll() throws DaoException {
    Collection<Entity> agences = new ArrayList<>();
    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM agence");
      while (resultSet.next())
        agences.add(new Agence(
            resultSet.getInt("idagence"),
            resultSet.getInt("nbemployes"),
            (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt("idville"))
        ));
    } catch (SQLException e) {
      throw new DaoException(e);
    }
    return agences;
  }

  @Override
  public Entity findById(int id) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("SELECT * FROM agence WHERE idagence = ? LIMIT 1");
      statement.setInt(1, id);
      ResultSet resultSet =statement.executeQuery();
      resultSet.next();
      return new Agence(
          resultSet.getInt("idagence"),
          resultSet.getInt("nbemployes"),
          (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt("idville"))
      );
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void create(Entity entity) throws DaoException {
    Agence agence = (Agence) entity;
    try {
      PreparedStatement statement = connection.prepareStatement(
          "INSERT INTO agence(nbemployes, idville) VALUES (?, ?)"
      );
      statement.setInt(1, agence.getNbEmployes());
      statement.setInt(2, agence.getVille().getIdVille());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne insérée");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void update(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          "UPDATE agence SET nbemployes = ?, idville = ? WHERE idagence = ?"
      );
      Agence agence = (Agence) entity;
      statement.setInt(1, agence.getNbEmployes());
      statement.setInt(2, agence.getVille().getIdVille());
      statement.setInt(3, agence.getIdAgence());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne modifié");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  @Override
  public void delete(Entity entity) throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement("DELETE FROM agence WHERE idagence = ?");
      statement.setInt(1, ((Agence) entity).getIdAgence());
      if (statement.executeUpdate() > 0)
        System.out.println("Ligne supprimé");
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }

  public Map<Agence, Double> turnoverByAgence() throws DaoException {
    try {
      PreparedStatement statement = connection.prepareStatement(
          """
          SELECT a.*, SUM(f.montant)
          FROM agence a
          INNER JOIN contrat c
          ON c.idagencederetour = a.idagence
          INNER JOIN facture f
          ON c.idcontrat = f.idcontrat
          WHERE c.datederetour > '2019-12-31' AND c.datederetrait < '2021-01-01'
          GROUP BY a.idagence
          """
      );
      ResultSet resultSet = statement.executeQuery();
      Map<Agence, Double> map = new HashMap<>();
      while (resultSet.next())
        map.put(new Agence(
            resultSet.getInt(1),
            resultSet.getInt(2),
            (Ville) new VilleDaoImpl(connection).findById(resultSet.getInt(3))
        ), resultSet.getDouble(4));
      return map;
    } catch (SQLException e) {
      throw new DaoException(e);
    }
  }
}
