\i sql/DROP_TABLES.sql

create table ville
(
	idville serial not null,
	nomville varchar,
	nombrehabitants int default 0
);

create unique index ville_idville_uindex
	on ville (idville);

alter table ville
	add constraint ville_pk
		primary key (idville);

create table agence
(
	idagence serial not null,
	nbemployes int default 0,
	idville int
		constraint agence_ville_idville_fk
			references ville
);

create unique index agence_idagence_uindex
	on agence (idagence);

alter table agence
	add constraint agence_pk
		primary key (idagence);

create table marque
(
	idmarque serial not null,
	nommarque varchar
);

create unique index marque_idmarque_uindex
	on marque (idmarque);

alter table marque
	add constraint marque_pk
		primary key (idmarque);

create table client
(
	idclient serial not null,
	nomclient varchar,
	adresseclient varchar,
	codepostalclient int,
	idville int
		constraint client_ville_idville_fk
			references ville
);

create unique index client_idclient_uindex
	on client (idclient);

alter table client
	add constraint client_pk
		primary key (idclient);

create table type
(
	idtype serial not null,
	libelletype varchar
);

create unique index type_idtype_uindex
	on type (idtype);

alter table type
	add constraint type_pk
		primary key (idtype);

create table categorie
(
	idcategorie serial not null,
	libellecategorie varchar
);

create unique index categorie_idcategorie_uindex
	on categorie (idcategorie);

alter table categorie
	add constraint categorie_pk
		primary key (idcategorie);

create table modele
(
	idmodele serial not null,
	denomination varchar,
	puissancefiscale int
);

create unique index modele_idmodele_uindex
	on modele (idmodele);

alter table modele
	add constraint modele_pk
		primary key (idmodele);

create table vehicule
(
	immatriculation varchar not null,
	datemiseencirculation date,
	etat varchar,
	nbkilometres int,
	prixparjourdelocation float,
	idmarque int
		constraint vehicule_marque_idmarque_fk
			references marque,
	idmodele int
		constraint vehicule_modele_idmodele_fk
			references modele,
	idcategorie int
		constraint vehicule_categorie_idcategorie_fk
			references categorie,
	idtype int
		constraint vehicule_type_idtype_fk
			references type,
	idagence int
		constraint vehicule_agence_idagence_fk
			references agence
);

create unique index vehicule_immatriculation_uindex
	on vehicule (immatriculation);

alter table vehicule
	add constraint vehicule_pk
		primary key (immatriculation);

create table contrat
(
	idcontrat serial not null,
	datederetrait date,
	datederetour date,
	kmretrait int,
	kmretour int,
	idclient int
		constraint contrat_client_idclient_fk
			references client,
	immatriculation varchar
		constraint contrat_vehicule_immatriculation_fk
			references vehicule,
	idagencederetour int
		constraint contrat_agence_idagence_fk
			references agence
);

create unique index contrat_idcontrat_uindex
	on contrat (idcontrat);

alter table contrat
	add constraint contrat_pk
		primary key (idcontrat);

create table facture
(
	idfacture serial not null,
	montant float,
	idcontrat int
		constraint facture_contrat_idcontrat_fk
			references contrat
);

create unique index facture_idfacture_uindex
	on facture (idfacture);

alter table facture
	add constraint facture_pk
		primary key (idfacture);

