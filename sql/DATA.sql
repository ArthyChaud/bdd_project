/* INSERT DATA ville */
insert into ville values(default, 'Paris', 10784830);
insert into ville values(default, 'Marseille', 861635);
insert into ville values(default, 'Toul', 16021);
insert into ville values(default, 'Strasbourg', 277270);
insert into ville values(default, 'Belfort', 49519);
insert into ville values(default, 'Nice', 942886);
insert into ville values(default, 'Toulon', 167479);
insert into ville values(default, 'Bordeaux', 249712);
insert into ville values(default, 'Toulouse', 968648);
insert into ville values(default, 'Besancon', 116676);

/* INSERT DATA agence */
insert into agence values(default, 1000, 1);
insert into agence values(default, 2000, 2);
insert into agence values(default, 3000, 3);
insert into agence values(default, 4000, 4);
insert into agence values(default, 5000, 5);
insert into agence values(default, 6000, 6);
insert into agence values(default, 7000, 7);
insert into agence values(default, 8000, 8);
insert into agence values(default, 9000, 9);
insert into agence values(default, 10000, 10);

/* DATA marque */ 
insert into marque values(default, 'Peugeot');
insert into marque values(default, 'Renault');
insert into marque values(default, 'Opel');
insert into marque values(default, 'Citroen');
insert into marque values(default, 'Jeep');
insert into marque values(default, 'Tesla');

/* DATA client */ 
insert into client values(default, 'maxime', '31 rue corne d amont Vy-lès-lure',70200,1 );
insert into client values(default, 'arthur', '7 bis rue du pré de l etang',70000,2 );
insert into client values(default, 'theo', '31 rue corne d amont Vy-lès-lure',70200,3 );
insert into client values(default, 'fabien', '1 rue des trois freres',75018,4 );
insert into client values(default, 'leo', '31 rue corne d amont Vy-lès-lure',70200,5 );


/* DATA categorie */ 
insert into categorie values(default,'berline');
insert into categorie values(default,'cabriolet');
insert into categorie values(default,'SUV');

/* DATA type */
insert into type values (default, 'familiale');
insert into type values (default, 'sport');
insert into type values (default, 'autre');

/* DATA modele */ 
insert into modele values(default,'3008',50);
insert into modele values(default,'Megane',80);
insert into modele values(default,'GLC',40);

/* DATA vehicule */ 
insert into vehicule values('AK001AA', '2020-02-23', 'BON', 50000, 70.0, 1, 1, 1, 1, 1);
insert into vehicule values('CV001AA', '2005-02-23', 'MAUVAIS', 50000, 10.0, 2, 2, 2, 2, 2);
insert into vehicule values('DT001AA', '2008-02-23', 'MOYEN', 100000,40.0, 3, 3, 3, 3, 3);
insert into vehicule values('BF001AA', '2020-02-23', 'NEUF', 1000, 120.0,1, 1, 1, 1, 4);
insert into vehicule values('AC001AA', '2018-02-23', 'EXCELLENT', 10000, 230.0, 2, 2, 2, 2, 5);
insert into vehicule values('AJ001AA', '2014-02-23', 'BON', 75000, 50.0, 3, 3, 3, 3, 6);
insert into vehicule values('AA001AA', '2000-12-11', 'MAUVAIS', 400000, 20.5, 2, 1, 1, 3, 1);
insert into vehicule values('BB010BB', '2012-01-01', 'MOYEN', 230000, 44.0, 3, 1, 2, 1, 2);
insert into vehicule values('CC011CC', '1970-01-01', 'MAUVAIS', 666000, 30.0, 2, 1, 1, 3, 3);
insert into vehicule values('AJ001AB', '2014-02-23', 'BON', 75000, 50.0, 3, 3, 3, 3, 2);
insert into vehicule values('DT001AB', '2008-02-23', 'MOYEN', 100000,40.0, 3, 3, 3, 3, 2);

/* DATA contrat */ 
insert into contrat values(default,'2020-02-23','2020-02-24',1000,1100,1,'AK001AA',5);
insert into contrat values(default,'2020-01-20','2020-01-24',2000,200,2,'CV001AA',2);
insert into contrat values(default,'2020-03-23','2020-03-28',30000,30050,3,'DT001AA',4);
insert into contrat values(default,'2020-05-24','2020-06-01',11000,11500,4,'BF001AA',3);
insert into contrat values(default,'2020-02-10','2020-02-21',42000,42200,5,'AJ001AA',1);

/* DATA facture */ 
insert into facture values(default,50000.0,1);
insert into facture values(default,5000.0,2);
insert into facture values(default,1200.0,3);
insert into facture values(default,700.0,4);
insert into facture values(default,300.0,5);